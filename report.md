# Graphique et visualisation - Projet

**Groupe** :

* Thomas Cottin
* Léo Donny
* Clément Garin

## Présentation du projet

Sign Aphabet est un projet ayant pour but l'apprentissage ludique de la langue des signes. Il exploite la technologie Leamotion, permettant de récupérer la position d'une ou plusieurs mains dans l'espace.

Le protorype actuel est limité à l'apprentissage des lettres dites "non-dynamique", c'est-à-dire des lettres qui ne nécessitent pas de bouger la main. 

En mode apprentissage, l'utilisateur a en face de lui une image, avec le signe à reproduire. En fonction du placement de sa main, les différentes parties de celle-ci deviennent vertes pour indiquer un placement correct, ou rouge dans le cas contraire. Une fois que la main est correctement placée, l'utilisateur doit maintenir la position pendant 3 secondes.

### Features du projet

#### Majeurs

* Écran titre avec interaction clavier / souris
* Mode apprentissage
* Lettres "statiques" (sans gestes, ex. 'B')

#### Mineurs / Improvements

* Écran titre avec interaction Leapmotion
* Scoring sur la précision du geste
* Gestion des lettres "dynamiques" (avec gestes, ex. 'J')
* Mode competition

## Installation

Le projet s'exécute à l'aide de l'outil Processing.

Un Leapmotion est nécessaire. La librairie *Leap Motion for Processing* développée par Darius Morawiec est nécessaire. Elle s'installe via le Contribution Manager de l'outil.

## Analyse et interview

Le projet se base sur le constat suivant: apprendre le langage des signes seul est possible, mais ne garanti pas que les différents gestes sont correctement exécutés. 

De plus, pratiquer le langage des signes "dans le vide" n'est pas très motivant. C'est ce qui explique en partie l'utilisation du Leapmotion. L'intéraction avec l'utilisateur est très engagente, voir même ludique.

Sur un total de 5 personnes, la totalité ne sait pas signer mais serait intéréssée pour appendre les bases. Gobalement, ils attendent d'une application d'apprentissage qu'elle soit facile à prendre en main. Concernant l'utilisation du LeapMotion, aucune des personnes interrogées ne connait le périphérique. Après une démonstration basique des capacités du Leapmotion, la totalité du panel trouve son utilisation ludique.

L'application finale n'a pas été testée sur les utilsateurs lambdas.

## Prototypage

Avant de lancer le développement, nous avons conçu des mockups via l'outil Balsamiq Mockup. Ils sont disponibles dans un PDF fournit avec le dossier.

## Fonctionnement

### Ergnonomie

Afin de garder l'aspect ludique pendant la totalité de l'utilisation, l'application est entièrement contrôlable avec le Leapmotion. En effet, il est possible d'intéragir avec les différents boutons en placant l'index dessus. Une jauge commence à se charger pour indiquer à l'utilisateur qu'il est en train d'actionner le bouton. Une fois la jauge remplie, l'action liée au bouton est exécutée.

### Validation du geste

*Modèle = position à reproduire avec la main, représentant une lettre du language des signes*

Pour valider le modèle effectué par l'utilisateur deux solution :

**Comparaison à un pseudo-modèle** : 

on défini le modèle à reproduire et on le considère comme étant à l'echelle 1:1. Lorsqu'une personne veut essayer l'application, elle doit d'abord passer une phase de calibration en présentant sa main face au Leapmotion. Ainsi, on peut  calculer les différences avec le modèle 1:1 en comparant les extrémités des os de la main.

Il faut donc définir un modèle relativement précis, en prenant en compte les différentes positions des doigts dans l'espace.

**Comparaison via les angles** :

Plus simple, le modèle est représenté par des angles entre les différentes phalanges. Le modèle n'est donc pas lié à la taille de la main. C'est la solution retenue. Les angles sont stockés dans une classe qui est ensuite sérialisée dans un fichier. 

## Etat final du projet

Le projet actuel est fonctionnel.

### Capacités acquises

Aucune

### Difficultés rencontrées

Diverses difficultés ont été rencontrées durant ce projet.

La première est liée au Leapmotion. Bien que cet outil ouvre de nouvelles possibilités d'interaction homme-machine, sa précision n'est pas optimale. En effet, la détection est faite par divers capteurs centralisés sur un petit boitier : la reconstitution 3D est donc imprécise.

Cette difficulté est augmentée par l'API permettant de communiquer entre le Leapmotion et Processing. En comparaison avec l'API Java, celle-ci est moins fournie, réduisant les possibilités d'exploitation du boitier.

Concernant Processing, ce logiciel / langage est bien adapté à des utilisateurs non-développeurs ou débutants ; mais des développeurs plus expérimentés y trouveront rapidement des limitations. L'IDE est sommaire, peu ergonomique, certains mécanismes ne le sont pas non plus. Il est de plus impossible de gérer proprement son code : il n'y a pas de système de package, et le mécanisme sous-jacent à Processing fait la part belle aux classes internes, rendant impossible le fait d'établir une architecture "propre".
