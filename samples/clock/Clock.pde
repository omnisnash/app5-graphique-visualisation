class Clock
{
  Location currentLocation;
  float clockDiameter;
  float secondsRadius, minutesRadius, hoursRadius;
  float cx, cy;
  ArrayList<Location> locations;
  
  Clock(ArrayList<Location> locations, float clockDiameter, float secondsRadius, float minutesRadius, float hoursRadius, float cx, float cy)
  {
    this.locations = locations;
    this.currentLocation = locations.get(0);
    this.clockDiameter = clockDiameter;
    this.secondsRadius = secondsRadius;
    this.minutesRadius = minutesRadius;
    this.hoursRadius = hoursRadius;
    this.cx = cx;
    this.cy = cy;
  }
}

float BUTTON_POSITION_Y;
Clock clock;

void setup() { // App constructor
  
  BUTTON_POSITION_Y = height / 20;

  ArrayList<Location> locations = new ArrayList();

  Location london = new Location("London",-1,color(255,0,0));
  Location paris = new Location("Paris",0,color(0,255,0));
  Location newYork = new Location("New York",-6,color(0,0,255));
  
  locations.add(london);
  locations.add(paris);
  locations.add(newYork);
  
  int radius = min(width, height) / 4;
  float clockDiameter = radius * 1.8;
  float secondsRadius = radius * 0.72;
  float minutesRadius = radius * 0.60;
  float hoursRadius = radius * 0.50;
  
  float cx = width / 2;
  float cy = height / 8 * 5;
  
  
  clock = new Clock(locations, clockDiameter, secondsRadius, minutesRadius, hoursRadius, cx, cy);
  
  
  buildLocationShapes();
  stroke(255);
  
  fullScreen();

}

void draw() {
  background(255, 0, 255);

  for(Location location : clock.locations)
  {
     PShape shape = location.shape;
     shape.setFill(location.color_);
     shape(shape);
  }
  
  
  // Draw the clock background
  fill(80);
  noStroke();
  ellipse(clock.cx, clock.cy, clock.clockDiameter, clock.clockDiameter);
  
  // Angles for sin() and cos() start at 3 o'clock;
  // subtract HALF_PI to make them start at the top
  float s = map(second(), 0, 60, 0, TWO_PI) - HALF_PI;
  float m = map(minute() + norm(second(), 0, 60), 0, 60, 0, TWO_PI) - HALF_PI; 
  float h = map(hour() + clock.currentLocation.timeShift + norm(minute(), 0, 60), 0, 24, 0, TWO_PI * 2) - HALF_PI;
  
  // Draw the hands of the clock
  stroke(255);
  strokeWeight(1);
  line(clock.cx, clock.cy, clock.cx + cos(s) * clock.secondsRadius, clock.cy + sin(s) * clock.secondsRadius);
  strokeWeight(2);
  line(clock.cx, clock.cy, clock.cx + cos(m) * clock.minutesRadius, clock.cy + sin(m) * clock.minutesRadius);
  strokeWeight(4);
  line(clock.cx, clock.cy, clock.cx + cos(h) * clock.hoursRadius, clock.cy + sin(h) * clock.hoursRadius);
  
  // Draw the minute ticks
  strokeWeight(2);
  beginShape(POINTS);
  for (int a = 0; a < 360; a+=6) {
    float angle = radians(a);
    float x = clock.cx + cos(angle) * clock.secondsRadius;
    float y = clock.cy + sin(angle) * clock.secondsRadius;
    vertex(x, y);
  }
  endShape();
  
  fill(255);
  textSize(32);
  textAlign(CENTER);
  for(Location location : clock.locations)
  {
    text(location.name, location.shapeX + location.shapeWidth/2, location.shapeY + location.shapeHeight / 2);
  }
}

void buildLocationShapes()
{
  float nbButton = clock.locations.size();
  float margin = width / 40;
  float btnWidth = ((width - 2 * margin) / nbButton);
  textAlign(CENTER);
  for(int i = 0; i < nbButton; i++)
  {
    float x = margin + i * btnWidth;
    PShape rectangle = createShape(RECT, x, BUTTON_POSITION_Y, btnWidth, height / 5) ;
    clock.locations.get(i).setShape(rectangle, x, BUTTON_POSITION_Y, btnWidth, height / 5);
  }
}

void mousePressed(){
  for(Location location : clock.locations)
  {
    if(mouseY > location.shapeY && mouseY < location.shapeY + location.shapeHeight && mouseX > location.shapeX && mouseX < location.shapeX + location.shapeWidth)
    {
      clock.currentLocation = location;
    }
  }
}