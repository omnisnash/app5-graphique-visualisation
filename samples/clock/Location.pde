class Location {
 
  String name;
  int timeShift;
  color color_;
  PShape shape; // useless: bundeled methods and attributes are empty: only filed when importing svg image
  float shapeX;
  float shapeY;
  float shapeWidth;
  float shapeHeight;
  
  Location(String name, int timeShift, color color_)
  {
    this.name = name;
    this.timeShift = timeShift;
    this.color_ = color_;
  }
  
  void setShape(PShape shape, float shapeX, float shapeY, float shapeWidth, float shapeHeight)
  {
    this.shape = shape;
    this.shapeX = shapeX;
    this.shapeY = shapeY;
    this.shapeWidth = shapeWidth;
    this.shapeHeight= shapeHeight;
  }
  
}