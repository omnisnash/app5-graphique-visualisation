import de.voidplus.leapmotion.*;

LeapMotion leap;
HandDrawer handDrawer;

void setup()
{
  size(800, 500);
  background(255);
  
  leap = new LeapMotion(this);
  handDrawer = new HandDrawer(this);
}

void draw()
{
  int fps = leap.getFrameRate();
  
  background(255);

  for(Hand hand : leap.getHands())
  {
  //  HandModel handModel = new HandModel();
    
  //  for(Finger finger : hand.getFingers())
  //  {
  //    BoneDetectionModel boneDetection = null;
  //    Bone previousBone = null;
  //    Bone currentBone = null;
      
  //    int nbBones = finger.getType() == 0 ? 2 : 3;
      
  //    nbBones = 2;
  //    for(int i = 0; i < nbBones; i++)
  //    {
  //      currentBone = finger.getBone(i);
        
  //      if(previousBone != null)
  //      {
  //        //boneDetection = AngleDetector.detect(finger, previousBone, currentBone);
  //        handModel.addBoneDetection(boneDetection);
  //      }
        
  //      previousBone = currentBone;
  //    }
      
  //    // Create a dependency model for last bone
  //    handModel.addBoneDetection(new BoneDetectionModel(currentBone, boneDetection));
      
  //  }
    
    handDrawer.draw(hand, true);
    //hand.draw(5);
  }
}

public void drawBones(Bone bone) {

  noFill();

  stroke(0,0,0);
  strokeWeight(1);

  PVector next = bone.getNextJoint();
  PVector prev = bone.getPrevJoint();

  //if(previous1 == null)
  //{
  //  previous1 = next;
  //}
  //else if(previous2 == null)
  //{
  //  previous2 = next;
  //}

  //if(previous1 != null && previous2 != null)
  //{
  //  // print angles
  //  double angle = Math.toDegrees(getAngle(previous1, previous2, prev));
  //  println("Angle :" + angle);

  //  // draw
  //  text(1, previous1.x, previous1.y);
  //  ellipse(previous1.x, previous1.y, 10, 10);
  //  text(2, previous2.x, previous2.y);
  //  ellipse(previous2.x, previous2.y, 10, 10);
  //  text(3, prev.x, prev.y);
  //  ellipse(prev.x, prev.y, 10, 10);

  //  if(angle > 240 && angle < 300)
  //  {
  //    stroke(0,255,0);
  //    strokeWeight(3);
  //  }

  //  previous1 = null;
  //  previous2 = null;
  //}

     beginShape(PConstants.LINES);
        if (g.is2D()) {
            //strokeWeight(15);
            vertex(next.x, next.y);
            vertex(prev.x, prev.y);
        } else {
          //strokeWeight(15);
            //stroke(0,0,255);
            vertex(next.x, next.y, next.z);
            vertex(prev.x, prev.y, prev.z);
        }
        endShape(PConstants.OPEN);
}