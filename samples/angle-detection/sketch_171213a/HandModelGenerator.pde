/**
  * Utils for hand model generation.
 **/
public class HandModelGenerator
{
  
  public HandModel generate(Hand hand)
  {
    HandModel handModel = new HandModel();
    
    for(Finger finger : hand.getFingers())
    {
      BoneDetectionModel boneDetection = null;
      Bone previousBone = null;
      Bone currentBone = null;
      
      int nbBones = finger.getType() == 0 ? 2 : 3;
      
      nbBones = 2;
      for(int i = 0; i < nbBones; i++)
      {
        currentBone = finger.getBone(i);
        
        if(previousBone != null)
        {
          boneDetection = detect(finger, previousBone, currentBone);
          handModel.addBoneDetection(boneDetection);
        }
        
        previousBone = currentBone;
      }
      
      // Create a dependency model for last bone
      handModel.addBoneDetection(new BoneDetectionModel(currentBone, boneDetection)); 
    }
    
    return handModel;
  }
  
  private BoneDetectionModel detect(Finger finger, Bone previousBone, Bone currentBone)
  {
    if(previousBone == null)
    {
      return null;
    }
    
    double angle = getAngle(previousBone.getPrevJoint(), previousBone.getNextJoint(), currentBone.getNextJoint());
    BoneDetectionModel detection = new BoneDetectionModel(angle, finger.getType(), previousBone.getType());
    
    return detection;
  }
   
  private Double getAngle(PVector p1, PVector p2, PVector p3)
  {
    Double alpha = Math.atan2(p1.y - p2.y, p1.x - p2.x);
    Double beta = Math.atan2(p3.y - p2.y, p3.x - p2.x);
    Double angle = (beta - alpha);
  
    //Correction de l'angle pour le resituer entre 0 et 2PI
    while (angle < 0.0 || angle > 2 * Math.PI)
    {
      if (angle < 0.0)
      {
          angle += 2 * Math.PI;
      }
      else if (angle > 2 * Math.PI) 
      {
        angle -= 2 * Math.PI;
      }
    }
    
    return angle;
  }
}