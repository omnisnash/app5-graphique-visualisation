/**
  * Utils for drawing hands.
 **/
public class HandDrawer
{
   private PApplet parent;
   
   public HandDrawer(PApplet parent)
   {
     this.parent = parent;
   }
   
   public void draw(Hand hand)
   {
      draw(hand, false); 
   }
   
   public void draw(Hand hand, HandModel comparisonModel, HandModel currentModel, double accuracy)
   {
     // TODO
      draw(hand, false); 
   }
   
   public void draw(Hand hand, boolean hightlightIndex)
   {
    for(Finger finger : hand.getFingers())
    {
      drawJoints(finger, 5, hightlightIndex);
      
      int nbBones = finger.getType() == 0 ? 2 : 3;

      for(int i = 0; i < nbBones; i++)
      {
        drawBone(finger.getBone(i));
      }      
    }
    
    drawPalm(hand);
      

  }

    public void drawPalm(Hand hand)
    {
      parent.fill(150,150,150, 150);
      this.parent.strokeWeight(10);
      
      PVector lastJointOfThumb = hand.getThumb().getProximalBone().getPrevJoint();
      PVector lastJointOfIndex = hand.getIndexFinger().getMetacarpalBone().getPrevJoint();
      PVector lastJointOfMiddle = hand.getMiddleFinger().getMetacarpalBone().getPrevJoint();
      PVector lastJointOfRing = hand.getRingFinger().getMetacarpalBone().getPrevJoint();
      PVector lastJointOfPinky = hand.getPinkyFinger().getMetacarpalBone().getPrevJoint();
      
      PVector lastJointOfThumb2 = hand.getThumb().getProximalBone().getNextJoint();
      PVector lastJointOfIndex2 = hand.getIndexFinger().getMetacarpalBone().getNextJoint();
      PVector lastJointOfMiddle2 = hand.getMiddleFinger().getMetacarpalBone().getNextJoint();
      PVector lastJointOfRing2 = hand.getRingFinger().getMetacarpalBone().getNextJoint();
      PVector lastJointOfPinky2 = hand.getPinkyFinger().getMetacarpalBone().getNextJoint  ();

      this.parent.beginShape();

      this.parent.vertex(lastJointOfThumb.x, lastJointOfThumb.y);
      this.parent.vertex(lastJointOfIndex.x, lastJointOfIndex.y);
      this.parent.vertex(lastJointOfMiddle.x, lastJointOfMiddle.y);
      this.parent.vertex(lastJointOfRing.x, lastJointOfRing.y);
      this.parent.vertex(lastJointOfPinky.x, lastJointOfPinky.y);
      
      this.parent.vertex(lastJointOfPinky2.x, lastJointOfPinky2.y);
      this.parent.vertex(lastJointOfRing2.x, lastJointOfRing2.y);
      this.parent.vertex(lastJointOfMiddle2.x, lastJointOfMiddle2.y);
      this.parent.vertex(lastJointOfIndex2.x, lastJointOfIndex2.y);
      this.parent.vertex(lastJointOfThumb2.x, lastJointOfThumb2.y);
    
      this.parent.endShape(PConstants.CLOSE);
    }

    public void drawJoints(Finger finger, float radius, boolean hightlightIndex)
    {
        this.parent.strokeWeight(10);
        this.parent.noStroke();
        this.parent.fill(0);
        
        PVector tip = finger.getPositionOfJointTip();
        PVector mcp = finger.getPositionOfJointMcp();
        PVector pip = finger.getPositionOfJointPip();
        PVector dip = finger.getPositionOfJointDip();

        this.parent.ellipseMode(PConstants.CENTER);
        this.parent.ellipse(pip.x, pip.y, radius, radius);
        this.parent.ellipse(dip.x, dip.y, radius, radius);
        this.parent.ellipse(tip.x, tip.y, radius, radius);
        
        if (finger.getType() != 0) 
        { // ≠thumb
            this.parent.ellipse(mcp.x, mcp.y, radius, radius);
        }
        
        if(hightlightIndex && finger.getType() == EFingerReferences.INDEX.getIndex())
        {
          this.parent.stroke(255, 0, 0);
          this.parent.strokeWeight(3);
          this.parent.noFill();
          this.parent.ellipse(tip.x, tip.y, 25, 25);
          this.parent.noStroke();
        }
    }
    
  public void drawBone(Bone bone)
  {
    this.parent.noFill();
  
    PVector next = bone.getNextJoint();
    PVector prev = bone.getPrevJoint();
  
    this.parent.noFill();
    this.parent.stroke(150,150,150,150);
    this.parent.strokeWeight(20);
    this.parent.strokeJoin(ROUND);
    
    this.parent.beginShape(PConstants.LINES);
    this.parent.vertex(next.x, next.y);
    this.parent.vertex(prev.x, prev.y);
    this.parent.endShape(PConstants.OPEN);
  }
}
 