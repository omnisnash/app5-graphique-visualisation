public enum EBoneReferences
{
    DISTAL       ("distal",       0),
    INTERMEDIATE ("intermediate", 1),
    PROXIMAL     ("proximal",     2),
    METACARPAL   ("metacarpal",   3);

    private String name;
    private int index;

    private EBoneReferences(String name, int index)
    {
        this.name = name;
        this.index = index;
    }

    public String getName()
    {
        return name;
    }

    public int getIndex()
    {
        return index;
    }
    
    public String toString()
    {
       return name; 
    }
}