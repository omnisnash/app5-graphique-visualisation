import de.voidplus.leapmotion.*;

import java.util.Map;

LeapMotion leap;

void settings()
{
  size(800, 500);
}

void setup()
{
  leap = new LeapMotion(this);
}

void draw()
{
  for (Hand hand : leap.getHands())
  {
    background(255, 255, 255);
    hand.draw();
    for (Finger finger : hand.getFingers())
    {
      Bone previousBone = null;
      Bone currentBone = null;

      int nbBones = finger.getType() == 0 ? 3 : 4;

      for (int i = 0; i < nbBones; i++)
      {
        currentBone = finger.getBone(i);

        if (previousBone != null)
        {
          detect(finger, previousBone, currentBone);
          break;
        }

        previousBone = currentBone;
      }

      break;
    }
  }
}

private void detect(Finger finger, Bone previousBone, Bone currentBone)
{
  if (previousBone == null)
  {
    return;
  }


  fill(255, 0, 0);
  ellipse(previousBone.getNextJoint().x, previousBone.getNextJoint().y, 5, 5);
  fill(0, 255, 0);
  ellipse(previousBone.getPrevJoint().x, previousBone.getPrevJoint().y, 5, 5);
  fill(0, 0, 255);
  ellipse(currentBone.getPrevJoint().x, currentBone.getPrevJoint().y, 5, 5);
  
  double angle = getAngle(previousBone.getNextJoint(), previousBone.getPrevJoint(), currentBone.getPrevJoint());
  println("!!!!!!!!!!!!!!!!!!" + angle);
}

private Double getAngle(PVector p1, PVector fixed, PVector p2)
{
  Double alpha = Math.atan2(p1.y - fixed.y, p1.x - fixed.x);
  Double beta = Math.atan2(p2.y - fixed.y, p2.x - fixed.x);
  Double angle = (beta - alpha);
  


  //Correction de l'angle pour le resituer entre 0 et 2PI
  while (angle < 0.0 || angle > 2 * Math.PI)
  {
    if (angle < 0.0)
    {
      angle += 2 * Math.PI;
    } else if (angle > 2 * Math.PI) 
    {
      angle -= 2 * Math.PI;
    }
  }

  return Math.toDegrees(angle);
}