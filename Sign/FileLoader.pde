import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Map;
import java.util.HashMap;
import java.util.regex.Pattern;

public static class FileLoader
{
 
  public static Map<String, HandModel> load(String stringPattern, String fileFolder)
  {
    Pattern pattern = Pattern.compile(stringPattern);
    
    Map<String, HandModel> letterMap = new HashMap<String, HandModel>();
    
    File folder = new File(fileFolder);
    if(!folder.exists())
    {
      try{
        folder.createNewFile();
      }
      catch(IOException e)
      {
      }
    }
    println(folder.getAbsolutePath());
    if(folder.listFiles() == null)
    {
      return letterMap;
    }
    for(File file: folder.listFiles())
    {
      if(pattern.matcher(file.getName()).matches())
      {
        try
        {
          ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
          HandModel o = (HandModel) ois.readObject();
          ois.close();
          letterMap.put(o.getLetter(), o);
          println("Note: " + file.getName() + " loaded as HandModel for letter " + o.getLetter());
        }
        catch(IOException e)
        {
          println("Error: " + file.getName() + " is not a valid HandModel object");
        }
        catch(ClassNotFoundException e)
        {
          println("Error: " + file.getName() + " is not a valid HandModel object");
        }
        catch(Exception e)
        {
          println("Error: " + file.getName() + " is not a valid HandModel object");
        }
      }
    }
    
    return letterMap;
  }
  
}