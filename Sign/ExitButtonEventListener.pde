import java.lang.Deprecated;

/*
 * Not used anymore
 * Represents an "Exit" button with click / released events triggered by an IButtonListener
 */
@Deprecated
class ExitButtonEventsListener implements IButtonListener
{
    private Button button;

    public ExitButtonEventsListener(Button button)
    {
        this.button = button;
    }

    public void onButtonPressed()
    {
        if(mouseX > button.getLocationX() && mouseX < button.getLocationX() + button.getWidth() && mouseY > button.getLocationY() && mouseY < button.getLocationY() + button.getHeight())
        {
            button.pressed();
        }
        else
        {
            button.released();
        }
    }

    public void onButtonReleased()
    {
        if(mouseX > button.getLocationX() && mouseX < button.getLocationX() + button.getWidth() && mouseY > button.getLocationY() && mouseY < button.getLocationY() + button.getHeight())
        {
            print("pressed");
            System.exit(0);
        }
        else
        {
            button.released();
        }
    }
}