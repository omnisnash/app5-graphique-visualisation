/**
 * Second scene: will represent the game
 */
class SecondaryScene implements IScene
{

    private TimeoutButton back;
    ArrayList<PVector> cursors = new ArrayList<PVector>();
    private String title;

    public SecondaryScene(String title)
    {
        this.title = title;
        int buttonsWidth = width/8;
        int buttonsHeight = height/15;

        int buttonX = buttonsWidth/2;
        int buttonY = buttonsHeight;
        back = new TimeoutButton(buttonX, buttonY, buttonsWidth, buttonsHeight, 1, "Back");
    }

    private void drawText()
    {
        textSize(64);
        fill(0);
        textAlign(CENTER);
        text(title, width/2, height/5);
    }

    public void draw()
    {
        background(0);
        cursors.clear();
        back.render();
        drawText();

        strokeWeight(3);
        noFill();
        for (Hand hand : leap.getHands())
        {
            Finger index = hand.getIndexFinger();
            PVector pos = index.getPosition();
            if (hand.isRight())
            {
                stroke(255, 0, 0);
            }
            else
            {
                stroke(0, 255, 0);
            }
            ellipse(pos.x, pos.y, 30, 30);
            cursors.add(pos);
        }

        back.update(cursors);

        if(back.showPress == 10)
        {
            currentDisplay = new MainTitle();
        }
    }

    /*
    public void mousePressed()
    {
    }
    */
    /*
    public void mouseReleased()
    {
    }
    */
}