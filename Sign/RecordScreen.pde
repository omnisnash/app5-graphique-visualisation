import java.io.*;
import java.util.Scanner;

class RecordScreen implements IScene
{ 
  private boolean eventLocked;
  
  float rect_x = width/4;
  float rect_y = height/4;
  float rect_w = width/2;
  float rect_h = width/4;
  
  float btn_w = width/8;
  float btn_h = width/16;
  float btn_x = width/2 - btn_w / 2;
  float btn_y = 9*height/10 - btn_h / 2;
  
  public RecordScreen()
  {
    eventLocked = false;
  }
  
  void saveModel(Hand hand)
  {
    HandModelGenerator generator = new HandModelGenerator();
    HandModel handModel = generator.generate(hand);
    
    if(!Character.isLetter(key))
    {
        println("Error: No letter for the current save. Press a keyboard key letter.");
        return;
    }

    handModel.setLetter("" + key);
    
    ObjectOutputStream oos = null;

    try {
      final FileOutputStream fichier = new FileOutputStream(sketchPath(RESOURCE_DIR_MODEL + key + ".ser"));
      oos = new ObjectOutputStream(fichier);
      oos.writeObject(handModel);
    } catch (final java.io.IOException e) {
      e.printStackTrace();
    } finally {
      try {
        if (oos != null) {
          oos.flush();
          oos.close();
          
          println("Hand '"+key+"' saved in " + sketchPath("resources/" + key + ".ser"));
        }
      } catch (final IOException ex) {
        ex.printStackTrace();
      }
    }
  }
  
  void manageBtnClick(Hand hand)
  {
    if(mousePressed)
    {
      if(!eventLocked && mouseX > btn_x && mouseX < btn_x + btn_w && mouseY > btn_y && mouseY < btn_y + btn_h)
      {
        eventLocked = true;
        saveModel(hand);
        eventLocked = false;
      }
    }
  }
  
  void draw(){
    
    background(0);
    
    fill(255);
    rect(rect_x, rect_y, rect_w, rect_h);
    
    for(Hand hand : leap.getHands())
    {
      HandDrawer drawer = new HandDrawer();
      drawer.draw(hand);
      manageBtnClick(hand);
    }
    
    fill(255);
    rect(btn_x, btn_y, btn_w, btn_h);
    fill(0);
    textAlign(CENTER, CENTER);
    text("Record", btn_x + btn_w/2, btn_y + btn_h/2);
  }

}