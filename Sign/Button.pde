import java.lang.Deprecated;

/**
 * Not used anymore
 * Represents a button with click / released events triggered by an IButtonListener
 */
@Deprecated
class Button
{
    
    private float location_x;
    private float location_y;
    private float btn_width;
    private float btn_height;
    private color backgroundColor;
    private color selectedColor;
    private color currentColor;
    private IButtonListener buttonEvents;

    public Button(float location_x, float location_y, float btn_width, float btn_height, color backgroundColor, color selectedColor)
    {
        this.location_x = location_x;
        this.location_y = location_y;
        this.btn_width = btn_width;
        this.btn_height = btn_height;
        this.backgroundColor = backgroundColor;
        this.selectedColor = selectedColor;
        this.currentColor = backgroundColor;
    }

    public void draw()
    {
        fill(currentColor);
        rect(location_x, location_y, btn_width, btn_height);
    }

    public void setButtonEventsListener(IButtonListener buttonEvents)
    {
        this.buttonEvents = buttonEvents; 
    }

    public void pressed()
    {
        currentColor = selectedColor;
    }

    public void released()
    {
        currentColor = backgroundColor;
    }

    public void buttonPressed()
    {
        buttonEvents.onButtonPressed();
    }

    public void buttonReleased()
    {
        buttonEvents.onButtonReleased();
    }

    public float getLocationX()
    {
        return location_x;
    }

    public float getLocationY()
    {
        return location_y;
    }

    public float getWidth()
    {
        return btn_width;
    }

    public float getHeight()
    {
        return btn_height;
    }

}