import java.util.Set;
import java.util.Collections;
import java.util.Date;

class GameScene implements IScene {
  
  // Set up
  // interesting points
  private float rx = 0;
  private float ry = 0;
  private float rw = 0;
  private float rh = 0;

  // Colors
  private int R_INIT = 255;
  private int G_INIT = 255;
  private int B_INIT = 255;

  private int R_FLOOR = 219;
  private int G_FLOOR = 197;
  private int B_FLOOR = 168;
  
  private int R_WALL = 224;
  private int G_WALL = 224;
  private int B_WALL = 224;
  
  private int R_TEXT = 0;
  private int G_TEXT = 102;
  private int B_TEXT = 153;
  
  // Floor texture
  private PImage texture = loadImage("./resources/img/parquet.jpg");

  // Letter
  private char currentLetter;
  private PShape currentLetterImg;

  // Timer
  private long startTs = -1;
  private int timer = 3;
  private long GGStartTs = -1;
  private int GGTimer = 2;
  private String GGText = "BRAVO !";

  private int total = 0;
  
  private String name;
  
  private boolean isPlaying = false;
  private boolean isHandOk = false;
  
  private HandComparator comparator;
  private HandModel currentModel = null;
  
  private TimeoutButton back;
  private TimeoutButton pass;
  private ArrayList<PVector> cursors = new ArrayList<PVector>();
 
  // Constructor
  public GameScene() {
    // set interesting points
    this.rx = width/4;
    this.ry = height/8;
    this.rw = width/2;
    this.rh = height/16 * 8;
    
    this.isPlaying = true;
    this.changeLetter();
    
    this.comparator = new HandComparator();
    
    // Build back button
    int buttonsWidth = width/8;
    int buttonsHeight = height/15;
    int buttonX = buttonsWidth/2;
    int buttonY = height/4;
    back = new TimeoutButton(buttonX, buttonY, buttonsWidth, buttonsHeight, 1, "Back", color(113,0,0));
    pass = new TimeoutButton(width - buttonsWidth - buttonsWidth/2, buttonY, buttonsWidth, buttonsHeight, 1, "Pass", color(0,152,185));

    
  }
  
  public GameScene(String name) {
     this();
     this.name = name;
     
     // TODO window name ?
  }
  
  void validateMove() {
    this.total++;
    this.changeLetter();
    this.startTs = -1;
    this.GGStartTs = -1;
  }

  boolean isHandValid() {
    for (Hand hand : leap.getHands ())
    {
        boolean same = comparator.printAndCompare(hand, currentModel , DETECTION_SENSIBILITY);
        
        if(same)
        {
           return true;
        }
    }
    
    return false;
  }

  void changeLetter() {
    List<String> keys = new ArrayList<String>(letters.keySet());
    Collections.shuffle(keys);
    this.currentLetter = keys.get(0).charAt(0);
    this.currentLetterImg = loadShape(RESOURCE_DIR_IMAGE_MODEL + this.currentLetter + ".svg");
    currentModel = letters.get(keys.get(0));
    this.isPlaying = true;
  }
  
  void draw() {
    background(0); 
       
    // init
    fill(R_INIT, G_INIT, B_INIT);
    stroke(0, 0, 0);
    strokeWeight(2);
    rect(rx, ry, rw, rh);

    
    // floor 2D
    /* fill(R_FLOOR, G_FLOOR, B_FLOOR);
    beginShape();
    texture(texture);
    vertex(rx, ry + rh, rx, 0);
    vertex(rx + rw, ry + rh, 1329-rx, 0);
    vertex(width, height / 4 * 3, width, height / 4 * 3);
    vertex(width, height, 1329, 709);
    vertex(0, height, 0, 709);
    vertex(0, height  / 4 * 3, 0, height  / 4 * 3);
    endShape(CLOSE); */
   
    // floor 3D - need to draw it first because it's not a "shape" but a square in a 3D scene
    textureWrap(REPEAT);
    beginShape();
    texture(texture);
    vertex(-1000, ry + rh, -6000, 0, 0);
    vertex(width+1000, ry + rh, -6000, width, 0);
    vertex(width+1000, height, 0, width, height);
    vertex(-1000, height, 0, 0, height);
    endShape(CLOSE);
    
    // left wall
    fill(R_WALL, G_WALL, B_WALL);
    beginShape();
    vertex(0, 0);
    vertex(rx, ry);
    vertex(rx, ry + rh);
    vertex(0, height  / 32 * 26);
    endShape(CLOSE);
    
    // right wall
    fill(R_WALL, G_WALL, B_WALL);
    beginShape();
    vertex(width, 0);
    vertex(width, height  / 32 * 26);
    vertex(rx + rw, ry + rh);
    vertex(rx + rw, ry);
    endShape(CLOSE);
    
    // top
    fill(R_WALL, G_WALL, B_WALL);
    beginShape();
    vertex(0, 0);
    vertex(width, 0);
    vertex(rx + rw, ry);
    vertex(rx, ry);
    endShape(CLOSE);
    
    // Image Model
    shape(this.currentLetterImg, rx + rw/3, ry, rw/3, rh/8*5);
    
    // Text counter
    textMode(SHAPE); // for 3D rendering
    textSize(32);
    textAlign(CENTER, CENTER);
    fill(R_TEXT, G_TEXT, B_TEXT);
    text("Total : " + this.total + " sign(s)", width/2, rh/8, 0); 
    
    // Drawn return button
    back.render();
     pass.render();
     
    cursors.clear();
     
     for (Hand hand : leap.getHands())
        {
            Finger index = hand.getIndexFinger();
            PVector pos = index.getPosition();
            cursors.add(pos);
        }

      back.update(cursors);
      pass.update(cursors);
     
     

    
    this.isHandOk = this.isHandValid();

    
    if(isHandOk) {
        if(this.startTs == -1) {
            this.startTs = (new Date()).getTime();
        }
        long currentTime = (new Date()).getTime();
        int value = (int)(currentTime - this.startTs);
        value /= 1000;
        value = timer - value < 0 ? 0 : timer - value;

        // Text timer
        if (value != 0) {
          textMode(SHAPE); // for 3D rendering
          textSize(90);
          fill(R_TEXT, G_TEXT, B_TEXT);
          textAlign(CENTER, TOP);
          text("Wait... " + value + "s" , width/2, rh, 0);
          
        } else {
          this.isPlaying = false;
        }
     } else {
        this.startTs = -1;
     }
     
     if(!this.isPlaying) {
          textMode(SHAPE); // for 3D rendering
          textSize(90);
          fill(R_TEXT, G_TEXT, B_TEXT);
          textAlign(CENTER, TOP);
          text(GGText, width/2, rh, 0);
          
          if(this.GGStartTs == -1) {
            this.GGStartTs = (new Date()).getTime();
          }
          
          long currentGGTime = (new Date()).getTime();
          int valueGG = (int)(currentGGTime - this.GGStartTs);
          valueGG /= 1000;
          valueGG = GGTimer - valueGG < 0 ? 0 : GGTimer - valueGG;
          
          if (valueGG == 0) {
            this.validateMove();
          } 
     }
     
     if(back.showPress == 10)
      {
          currentDisplay = new MainTitle();
      }
      
      if(pass.showPress == 10)
      {
        this.changeLetter();
      }
     
     
  }
}