class Letter {

  private char letter;
  // TODO private model xxx
  static final String path = "../res/letters/";
  
  public Letter(char l) {
    this.letter = l;
  }
  
  public String getPath() {
    return this.path + this.letter + ".svg";
  }
  
  public char getLetter() {
    return this.letter;
  }
}