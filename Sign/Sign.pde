 import de.voidplus.leapmotion.*;

import java.util.Map;

static final boolean    ENV_PRODUCTION = true;
static final boolean    ENV_RECORD = false; // True : enables the record scene
static final String     RESOURCE_DIR_IMAGE = "./resources/img/";
static final String     RESOURCE_DIR_IMAGE_MODEL = RESOURCE_DIR_IMAGE + "model/";
static final String     RESOURCE_DIR_MODEL = "./resources/model/";
static final double     DETECTION_SENSIBILITY = 150.0;
static final int        SCREEN_WIDTH = 800;
static final int        SCREEN_HEIGHT = 600;
static final String     letterImgPath = "./resources/img/";



IScene currentDisplay;
Map<String, HandModel> letters;
LeapMotion leap;

/**
 * Main code. Entry point for the program
 */
 
void settings() {
  if(ENV_PRODUCTION){
    fullScreen(P3D);
  } else  {
    size(SCREEN_WIDTH, SCREEN_HEIGHT, P3D);
  }
}

void setup() {
  // Load serialized objects
  letters = FileLoader.load("[a-z].ser", sketchPath() + "/" + RESOURCE_DIR_MODEL);
  
  // Init LeapMotion controler
  leap = new LeapMotion(this);
  
  // Select first scene
  if(ENV_RECORD) {
    currentDisplay = new RecordScreen();
  } else {
    currentDisplay = new MainTitle("Sign Alphabet");
  }
}

void draw() {
    currentDisplay.draw();
}