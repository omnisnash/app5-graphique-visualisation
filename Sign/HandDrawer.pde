/**
 * Utils for drawing hands.
 */
public class HandDrawer
{
  static final int        DEFAULT_JOINT_RADIUS = 8;
  static final int        DEFAULT_TRANSPARENCY = 200;
  static final int        DEFAULT_STROKE_WEIGHT = 35;
  
    public HandDrawer()
    {
    }

    public void draw(Hand hand)
    {
        draw(hand, false); 
        
        noFill();
        noStroke();
    }

    public void draw(Hand hand, HandModel comparisonModel, HandModel currentModel, double accuracy)
    {
        for(Finger finger : hand.getFingers())
        {
            drawJoints(finger, DEFAULT_JOINT_RADIUS, false);

            int nbBones = finger.getType() == 0 ? 2 : 3;

            for(int i = 0; i < nbBones; i++)
            {
                drawBone(finger, finger.getBone(i), comparisonModel, currentModel, accuracy);
            }      
        }
        drawPalm(hand);
        
        noFill();
        noStroke();
    }

    public void draw(Hand hand, boolean hightlightIndex)
    {
        for(Finger finger : hand.getFingers())
        {
            drawJoints(finger, DEFAULT_JOINT_RADIUS, hightlightIndex);

            int nbBones = finger.getType() == 0 ? 2 : 3;

            for(int i = 0; i < nbBones; i++)
            {
                drawBone(finger.getBone(i));
            }      
        }
        drawPalm(hand);
        
        noFill();
        noStroke();
    }

    public void drawPalm(Hand hand)
    {
        fill(150,150,150, DEFAULT_TRANSPARENCY);
        strokeWeight(DEFAULT_STROKE_WEIGHT);

        PVector lastJointOfThumb = hand.getThumb().getProximalBone().getPrevJoint();
        PVector lastJointOfIndex = hand.getIndexFinger().getMetacarpalBone().getPrevJoint();
        PVector lastJointOfMiddle = hand.getMiddleFinger().getMetacarpalBone().getPrevJoint();
        PVector lastJointOfRing = hand.getRingFinger().getMetacarpalBone().getPrevJoint();
        PVector lastJointOfPinky = hand.getPinkyFinger().getMetacarpalBone().getPrevJoint();

        PVector lastJointOfThumb2 = hand.getThumb().getProximalBone().getNextJoint();
        PVector lastJointOfIndex2 = hand.getIndexFinger().getMetacarpalBone().getNextJoint();
        PVector lastJointOfMiddle2 = hand.getMiddleFinger().getMetacarpalBone().getNextJoint();
        PVector lastJointOfRing2 = hand.getRingFinger().getMetacarpalBone().getNextJoint();
        PVector lastJointOfPinky2 = hand.getPinkyFinger().getMetacarpalBone().getNextJoint  ();

        beginShape();

        vertex(lastJointOfThumb.x, lastJointOfThumb.y);
        vertex(lastJointOfIndex.x, lastJointOfIndex.y);
        vertex(lastJointOfMiddle.x, lastJointOfMiddle.y);
        vertex(lastJointOfRing.x, lastJointOfRing.y);
        vertex(lastJointOfPinky.x, lastJointOfPinky.y);

        vertex(lastJointOfPinky2.x, lastJointOfPinky2.y);
        vertex(lastJointOfRing2.x, lastJointOfRing2.y);
        vertex(lastJointOfMiddle2.x, lastJointOfMiddle2.y);
        vertex(lastJointOfIndex2.x, lastJointOfIndex2.y);
        vertex(lastJointOfThumb2.x, lastJointOfThumb2.y);

        endShape(PConstants.CLOSE);
    }

    public void drawJoints(Finger finger, float radius, boolean hightlightIndex)
    {
        strokeWeight(DEFAULT_STROKE_WEIGHT);
        noStroke();
        fill(200,200,200,DEFAULT_TRANSPARENCY - 50);

        PVector tip = finger.getPositionOfJointTip();
        PVector mcp = finger.getPositionOfJointMcp();
        PVector pip = finger.getPositionOfJointPip();
        PVector dip = finger.getPositionOfJointDip();

        //ellipseMode(PConstants.CENTER);
        
        sphereDetail(20);
        pushMatrix();
        translate(tip.x, tip.y, 0);
        sphere(radius);
        popMatrix();
        pushMatrix();
        translate(pip.x, pip.y, 0);
        sphere(radius);
        popMatrix();
        pushMatrix();
        translate(dip.x, dip.y, 0);
        sphere(radius);
        popMatrix();
        if (finger.getType() != 0) { // ≠thumb
          pushMatrix();
          translate(mcp.x, mcp.y, 0);
          sphere(radius);
          popMatrix();
        }

        if(hightlightIndex && finger.getType() == EFingerReferences.INDEX.getIndex())
        {
            stroke(255, 0, 0);
            strokeWeight(3);
            noFill();
            ellipse(tip.x, tip.y, 25, 25);
            noStroke();
        }
    }

    public void drawBone(Bone bone)
    {
        noFill();

        PVector next = bone.getNextJoint();
        PVector prev = bone.getPrevJoint();

        noFill();
        stroke(150,150,150,DEFAULT_TRANSPARENCY);
        strokeWeight(DEFAULT_STROKE_WEIGHT);
        strokeJoin(ROUND);

        beginShape(PConstants.LINES);
        vertex(next.x, next.y, 0);
        vertex(prev.x, prev.y, 0);
        endShape(PConstants.OPEN);
    }
    
    public void drawBone(Finger finger, Bone bone, HandModel comparison, HandModel current, double accuracy)
    {
        BoneDetectionModel comparisonBone = comparison.getBone(finger.getType(), bone.getType());
        BoneDetectionModel currentBone = current.getBone(finger.getType(), bone.getType());

        noFill();

        PVector next = bone.getNextJoint();
        PVector prev = bone.getPrevJoint();

        noFill();
        
        if(comparisonBone.compareTo(currentBone, accuracy))
        {
          stroke(0,150,0,DEFAULT_TRANSPARENCY);
        }
        else
        {
          stroke(150,0,0,DEFAULT_TRANSPARENCY);
        }
        
        strokeWeight(DEFAULT_STROKE_WEIGHT);
        strokeJoin(ROUND);

        beginShape(PConstants.LINES);
        vertex(next.x, next.y, 0);
        vertex(prev.x, prev.y, 0);
        endShape(PConstants.OPEN);
    }
    
}