import java.lang.Deprecated;

/**
 * Not used anymore
 * Interface for button listeners
 */
@Deprecated
interface IButtonListener
{
    public void onButtonReleased();
    public void onButtonPressed();
}