/**
 * Enum for fingers qualification
 */
public enum EFingerReferences
{
    THUMB  ("thumb",  0),
    INDEX  ("index",  1),
    MIDDLE ("middle", 2),
    RING   ("ringle", 3),
    PINKY  ("pinky",  4);

    private String name;
    private int index;

    private EFingerReferences(String name, int index)
    {
        this.name = name;
        this.index = index;
    }

    public String getName()
    {
        return name;
    }

    public int getIndex()
    {
        return index;
    }
}