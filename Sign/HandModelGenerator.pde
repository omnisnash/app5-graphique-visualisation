/**
  * Utils for hand model generation.
 **/
public class HandModelGenerator
{
  public HandModel generate(Hand hand)
  {
    HandModel handModel = new HandModel();
    
    for(Finger finger : hand.getFingers())
    {
      BoneDetectionModel boneDetection = null;
      Bone previousBone = null;
      Bone currentBone = null;
      
      int nbBones = finger.getType() == 0 ? 3 : 4;
      
      for(int i = 0; i < nbBones; i++)
      {
        currentBone = finger.getBone(i); //<>//
        
        if(previousBone == currentBone)
        {
           System.out.println("FUUUUUUUUUUUUUUUUUUUUUCK"); 
        }
        
        if(previousBone != null)
        {
          boneDetection = detect(finger, previousBone, currentBone);
          handModel.addBoneDetection(boneDetection);
        }
        
        previousBone = currentBone;
      }
      
      // Create a dependency model for last bone
      handModel.addBoneDetection(new BoneDetectionModel(currentBone.getType(), boneDetection)); 
    }
   
    
    return handModel;
  }
  
  private BoneDetectionModel detect(Finger finger, Bone previousBone, Bone currentBone)
  {
    if(previousBone == null)
    {
      return null;
    }
    
    double angle = getAngle(previousBone.getNextJoint(), previousBone.getPrevJoint(), currentBone.getPrevJoint());
    BoneDetectionModel detection = new BoneDetectionModel(angle, finger.getType(), previousBone.getType());
    
    return detection;
  }
   
  private Double getAngle(PVector p1, PVector fixed, PVector p2)
  {
    Double alpha = Math.atan2(p1.y - fixed.y, p1.x - fixed.x);
    Double beta = Math.atan2(p2.y - fixed.y, p2.x - fixed.x);
    Double angle = (beta - alpha); //<>//
  
    //Correction de l'angle pour le resituer entre 0 et 2PI
    while (angle < 0.0 || angle > 2 * Math.PI)
    {
      if (angle < 0.0)
      {
          angle += 2 * Math.PI;
      }
      else if (angle > 2 * Math.PI) 
      {
        angle -= 2 * Math.PI;
      }
    }
    
    return Math.toDegrees(angle);
  }
}