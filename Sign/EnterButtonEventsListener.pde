import java.lang.Deprecated;

/**
 * Not used anymore
 * Represents an "Enter" button with click / released events triggered by an IButtonListener
 */
@Deprecated
class EnterButtonEventsListener implements IButtonListener
{
    private Button button;

    public EnterButtonEventsListener(Button button)
    {
        this.button = button;
    }

    public void onButtonPressed()
    {
        if(mouseX > button.getLocationX() && mouseX < button.getLocationX() + button.getWidth() && mouseY > button.getLocationY() && mouseY < button.getLocationY() + button.getHeight())
        {
            button.pressed();
        }
        else
        {
            button.released();
        }
    }

    public void onButtonReleased()
    {
        if(mouseX > button.getLocationX() && mouseX < button.getLocationX() + button.getWidth() && mouseY > button.getLocationY() && mouseY < button.getLocationY() + button.getHeight())
        {
            currentDisplay = new SecondaryScene("Toto");
        }
        else
        {
            button.released();
        }
    }
}