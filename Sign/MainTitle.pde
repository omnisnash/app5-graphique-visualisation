/**
 * Represents the main title (aka menu) alowing user to play or exit
 * - many things commented for code history
 */
class MainTitle implements IScene
{
    private String title;
    //private Button enterButton, exitButton;
    private TimeoutButton enterButton, exitButton;
    private HandDrawer handDrawer;
    ArrayList<PVector> cursors = new ArrayList<PVector>();
    PImage logo;

    public MainTitle()
    {
        handDrawer = new HandDrawer();
        logo = loadImage(RESOURCE_DIR_IMAGE + "logo.png");
        //logo.resize(0, height);
        
        //enterButton = new Button(width/2 - width/16, height/2 - height/22.5, width/8, height/15, color(12,12,12), color(176,176,176));
        //exitButton = new Button(width/2 - width/16, height/2 + height/22.5, width/8, height/15, color(255,0,0), color(0,255,0));

        int buttonsWidth = width/4;
        int buttonsHeight = height/10;

        int firstButtonX = width/2 - buttonsWidth/2;
        int firstButtonY = height/2 - buttonsHeight/2;

        enterButton = new TimeoutButton(firstButtonX, firstButtonY, buttonsWidth, buttonsHeight, 1, "Play", color(0,152,185));
        exitButton = new TimeoutButton(firstButtonX, firstButtonY + buttonsHeight + buttonsHeight/2, buttonsWidth, buttonsHeight, 1, "Exit", color(113,0,0));

        /*
        IButtonListener enterButtonEventsListener = new EnterButtonEventsListener(enterButton);
        enterButton.setButtonEventsListener(enterButtonEventsListener);
        IButtonListener exitButtonEventsListener = new ExitButtonEventsListener(exitButton);
        exitButton.setButtonEventsListener(exitButtonEventsListener);
        */
    }

    public MainTitle(String title)
    {
        this();
        this.title = title;
    }

    private void drawText()
    {
        textSize(64);
        fill(0);
        textAlign(CENTER, CENTER);
        image(logo, width/2 - logo.width/2, height/4 - logo.height/2);
        //text("EXIT", exitButton.x + exitButton.w/2, exitButton.y + exitButton.h/2);
        //fill(255);
        //text("PLAY", enterButton.x + enterButton.w/2, enterButton.y + enterButton.h);
        
        textSize(16);
        fill(255);
        text("Polytech Paris-Sud - Thomas Cottin / Léo Donny / Clément Garin", width/2, height - height / 10);
    }

    private void drawButton()
    {
        //enterButton.draw();
        //exitButton.draw();
        enterButton.render();
        exitButton.render();
    }

    public void draw()
    {
        background(0);
        cursors.clear();
        drawButton();
        drawText();
        

        strokeWeight(3);
        noFill();
        
        for (Hand hand : leap.getHands())
        {
            handDrawer.draw(hand, true);
            Finger index = hand.getIndexFinger();
            PVector pos = index.getPosition();
            cursors.add(pos);
        }

        enterButton.update(cursors);
        exitButton.update(cursors);

        if(enterButton.showPress == 10)
        {
           background(0); 
           currentDisplay = new GameScene("");
        }
        if(exitButton.showPress == 10)
        {
            exit();
        }
    }
    /*
    public void mousePressed()
    {
        enterButton.buttonPressed();
        exitButton.buttonPressed();
    }
    */
    /*
    public void mouseReleased()
    {
        enterButton.buttonReleased();
        exitButton.buttonReleased();
    }
    */
}