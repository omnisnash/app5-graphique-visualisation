import java.util.List;

/**
 * Utils for hand comparison.
 */
public class HandComparator
{   
    private HandModelGenerator modelGenerator;
    private HandDrawer drawer;

    public HandComparator()
    {
        modelGenerator = new HandModelGenerator();
        drawer = new HandDrawer();
    }

    /**
    * Compare the user hand to a given model and display a virtual hand.
    * currentHand : the hand from the Leapmotion
    * model : the model used for the comparison
    * accuracy : angle acceptance (angle - accuracy < angle < angle + accuracy)
    **/
    public boolean printAndCompare(Hand currentHand, HandModel model, double accuracy)
    {
        // Create a model for the current hand
        HandModel currentModel = modelGenerator.generate(currentHand);
        //currentModel.debug();


        // Print the hand with color validation via the both models
        drawer.draw(currentHand, model, currentModel, accuracy);

        // Return the comparaison result
        return model.compareTo(currentModel, accuracy);
    }

}