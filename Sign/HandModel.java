/**
  * Serializable representation of a hand.
 **/
import java.util.*;
import java.io.Serializable;

public class HandModel implements Serializable
{
   private List<BoneDetectionModel> boneDetections;
   private String letter;
   
   public HandModel()
   {
    boneDetections = new ArrayList(); 
   }
   
   public void addBoneDetection(BoneDetectionModel detection)
   {
     boneDetections.add(detection);
   }
   
   public String getLetter()
   {
     return letter;
   }
   
   public void setLetter(String l)
   {
      letter = l; 
   }
   
   public BoneDetectionModel getBone(int finger, int bone)
   {
       for(BoneDetectionModel detection : boneDetections)
       {
          if(detection.getFinger() == finger && detection.getBone() == bone)
          {
             return detection;
          }
       }
       
       return null;
   }
   
   public boolean compareTo(HandModel other, double accuracy)
   {
       for(BoneDetectionModel detection : boneDetections)
       {
          if(!detection.compareTo(other.getBone(detection.getFinger(), detection.getBone()), accuracy))
          {
             return false;
          }
       }
       
      return true; 
   }
   
   public void debug()
   {
       for(BoneDetectionModel detection : boneDetections)
       {
          System.out.println(detection.getAngle()); 
       }
   }
}