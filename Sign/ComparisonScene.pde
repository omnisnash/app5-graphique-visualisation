class ComparisonScene implements IScene
{
    private HandComparator comparator;
    
    public ComparisonScene()
    {
        comparator = new HandComparator();
    }
    
    void draw()
    {
        background(0);
        
        
        
        for (Hand hand : leap.getHands ())
        {
            HandModel model = letters.get("p");
            //model.debug();
            
            boolean same = comparator.printAndCompare(hand, model , 60.0);
            println("Letter : " + model.getLetter() + " / " + same); 
            
            if(same)
            {
               System.out.println("OK");
            }
            else
            {
               System.out.println("NOPE"); 
            }
        }
    }
}