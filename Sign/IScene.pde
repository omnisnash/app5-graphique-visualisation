/**
 * Describes a scene 
 */
interface IScene
{
    void draw();

    //void mousePressed();

    //void mouseReleased();

}