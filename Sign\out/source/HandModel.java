/**
  * Serializable representation of a hand.
 **/
import java.util.*;
import java.io.Serializable;

public class HandModel implements Serializable
{
   private List<BoneDetectionModel> boneDetections;
   private String letter;
   
   public HandModel()
   {
    boneDetections = new ArrayList(); 
   }
   
   public void addBoneDetection(BoneDetectionModel detection)
   {
     boneDetections.add(detection);
   }
   
   public boolean compareTo(HandModel other, double accuracy)
   {
      // TODO
      return false; 
   }
   
   public String getLetter()
   {
     return letter;
   }
   
   public void setLetter(String l)
   {
      letter = l; 
   }
}