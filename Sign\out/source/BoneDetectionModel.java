import java.io.Serializable;

/**
  * Serializable representation of bone with it's associated angle.
 **/
public class BoneDetectionModel implements Serializable
{
  private double angle;
  private int finger;
  private int bone;
  private BoneDetectionModel dependency;
  
  public BoneDetectionModel(double angle, int finger, int bone)
  {
    this.angle = angle;
    this.finger = finger;
    this.bone = bone;
  }
  
  public BoneDetectionModel(int boneType, BoneDetectionModel dependency)
  {
    this.angle = dependency.angle;
    this.finger = dependency.finger;
    this.bone = boneType;
    
    setDependency(dependency);
  }
  
  public void setDependency(BoneDetectionModel dependency)
  {
    this.dependency = dependency;
  }
  
  public int getFinger() 
  { 
    return finger;
  }
  
  public int getBone() 
  { 
    return bone;
  }
  
  public double getAngle()
  {
   return dependency == null ? angle : dependency.getAngle();
  }
  
  
}