import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import de.voidplus.leapmotion.*; 
import java.util.Map; 
import java.lang.Deprecated; 
import java.lang.Deprecated; 
import java.lang.Deprecated; 
import java.io.BufferedInputStream; 
import java.io.FileInputStream; 
import java.io.ObjectInputStream; 
import java.util.Map; 
import java.util.HashMap; 
import java.util.regex.Pattern; 
import java.util.List; 
import java.lang.Deprecated; 
import java.io.*; 
import java.util.Scanner; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Sign extends PApplet {





IScene currentDisplay;
Map<String, Object> letters;
LeapMotion leap;

/**
 * Main code. Entry point for the program
 */
 
public void settings() {
  
  // For debug
   //fullScreen();
   size(800, 500);
}

public void setup()
{
  // Load serialized objects
  letters = FileLoader.load("[a-z].ser", sketchPath() + "/resources");
  
  leap = new LeapMotion(this);
  
  // currentDisplay = new MainTitle("Sign");
  // currentDisplay = new RecordScreen(this);
  //currentDisplay = new GameScene("Game");
  currentDisplay = new ComparisonScene(this);
  
  
}

public void draw()
{
    currentDisplay.draw();
}

public void mousePressed()
{
    //currentDisplay.mousePressed();
}

public void mouseReleased()
{
    //currentDisplay.mouseReleased();
}


/**
 * Not used anymore
 * Represents a button with click / released events triggered by an IButtonListener
 */
@Deprecated
class Button
{
    
    private float location_x;
    private float location_y;
    private float btn_width;
    private float btn_height;
    private int backgroundColor;
    private int selectedColor;
    private int currentColor;
    private IButtonListener buttonEvents;

    public Button(float location_x, float location_y, float btn_width, float btn_height, int backgroundColor, int selectedColor)
    {
        this.location_x = location_x;
        this.location_y = location_y;
        this.btn_width = btn_width;
        this.btn_height = btn_height;
        this.backgroundColor = backgroundColor;
        this.selectedColor = selectedColor;
        this.currentColor = backgroundColor;
    }

    public void draw()
    {
        fill(currentColor);
        rect(location_x, location_y, btn_width, btn_height);
    }

    public void setButtonEventsListener(IButtonListener buttonEvents)
    {
        this.buttonEvents = buttonEvents; 
    }

    public void pressed()
    {
        currentColor = selectedColor;
    }

    public void released()
    {
        currentColor = backgroundColor;
    }

    public void buttonPressed()
    {
        buttonEvents.onButtonPressed();
    }

    public void buttonReleased()
    {
        buttonEvents.onButtonReleased();
    }

    public float getLocationX()
    {
        return location_x;
    }

    public float getLocationY()
    {
        return location_y;
    }

    public float getWidth()
    {
        return btn_width;
    }

    public float getHeight()
    {
        return btn_height;
    }

}
class ComparisonScene implements IScene
{
    private PApplet env;
    private LeapMotion leap;
    private HandComparator comparator;
    
    public ComparisonScene(PApplet environement)
    {
        env = environement;
        leap = new LeapMotion(env);
        comparator = new HandComparator(env);
    }
    
    public void draw()
    {
        background(0);
        
        for (Hand hand : leap.getHands ())
        {
            comparator.printAndCompare(hand, null, 50.0f); 
        }
    }
}
/**
 * Enum describing bone types
 */
public enum EBoneReferences
{
    DISTAL       ("distal",       0),
    INTERMEDIATE ("intermediate", 1),
    PROXIMAL     ("proximal",     2),
    METACARPAL   ("metacarpal",   3);

    private String name;
    private int index;

    private EBoneReferences(String name, int index)
    {
        this.name = name;
        this.index = index;
    }

    public String getName()
    {
        return name;
    }

    public int getIndex()
    {
        return index;
    }

    public String toString()
    {
        return name; 
    }
}
/**
 * Enum for fingers qualification
 */
public enum EFingerReferences
{
    THUMB  ("thumb",  0),
    INDEX  ("index",  1),
    MIDDLE ("middle", 2),
    RING   ("ringle", 3),
    PINKY  ("pinky",  4);

    private String name;
    private int index;

    private EFingerReferences(String name, int index)
    {
        this.name = name;
        this.index = index;
    }

    public String getName()
    {
        return name;
    }

    public int getIndex()
    {
        return index;
    }
}


/**
 * Not used anymore
 * Represents an "Enter" button with click / released events triggered by an IButtonListener
 */
@Deprecated
class EnterButtonEventsListener implements IButtonListener
{
    private Button button;

    public EnterButtonEventsListener(Button button)
    {
        this.button = button;
    }

    public void onButtonPressed()
    {
        if(mouseX > button.getLocationX() && mouseX < button.getLocationX() + button.getWidth() && mouseY > button.getLocationY() && mouseY < button.getLocationY() + button.getHeight())
        {
            button.pressed();
        }
        else
        {
            button.released();
        }
    }

    public void onButtonReleased()
    {
        if(mouseX > button.getLocationX() && mouseX < button.getLocationX() + button.getWidth() && mouseY > button.getLocationY() && mouseY < button.getLocationY() + button.getHeight())
        {
            currentDisplay = new SecondaryScene("Toto");
        }
        else
        {
            button.released();
        }
    }
}


/*
 * Not used anymore
 * Represents an "Exit" button with click / released events triggered by an IButtonListener
 */
@Deprecated
class ExitButtonEventsListener implements IButtonListener
{
    private Button button;

    public ExitButtonEventsListener(Button button)
    {
        this.button = button;
    }

    public void onButtonPressed()
    {
        if(mouseX > button.getLocationX() && mouseX < button.getLocationX() + button.getWidth() && mouseY > button.getLocationY() && mouseY < button.getLocationY() + button.getHeight())
        {
            button.pressed();
        }
        else
        {
            button.released();
        }
    }

    public void onButtonReleased()
    {
        if(mouseX > button.getLocationX() && mouseX < button.getLocationX() + button.getWidth() && mouseY > button.getLocationY() && mouseY < button.getLocationY() + button.getHeight())
        {
            print("pressed");
            System.exit(0);
        }
        else
        {
            button.released();
        }
    }
}







public static class FileLoader
{
 
  public static Map<String, Object> load(String stringPattern, String fileFolder)
  {
    Pattern pattern = Pattern.compile(stringPattern);
    
    Map<String, Object> letterMap = new HashMap<String, Object>();
    
    File folder = new File(fileFolder);
    if(!folder.exists())
    {
      try{
        folder.createNewFile();
      }
      catch(IOException e)
      {
      }
    }
    println(folder.getAbsolutePath());
    if(folder.listFiles() == null)
    {
      return letterMap;
    }
    for(File file: folder.listFiles())
    {
      if(pattern.matcher(file.getName()).matches())
      {
        try
        {
          ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
          HandModel o = (HandModel) ois.readObject();
          ois.close();
          letterMap.put(file.getName(), o);
          println("Note: " + file.getName() + " loaded as HandModel for letter " + o.getLetter());
        }
        catch(IOException e)
        {
          println("Error: " + file.getName() + " is not a valid HandModel object");
        }
        catch(ClassNotFoundException e)
        {
          println("Error: " + file.getName() + " is not a valid HandModel object");
        }
        catch(Exception e)
        {
          println("Error: " + file.getName() + " is not a valid HandModel object");
        }
      }
    }
    
    return letterMap;
  }
  
}


/**
 * Utils for hand comparison.
 */
public class HandComparator
{   
    
    private PApplet env;
    private HandModelGenerator modelGenerator;
    private HandDrawer drawer;

    public HandComparator(PApplet environnement)
    {
        env = environnement;
        modelGenerator = new HandModelGenerator();
        drawer = new HandDrawer(env);
    }

    /**
    * Compare the user hand to a given model and display a virtual hand.
    * currentHand : the hand from the Leapmotion
    * model : the model used for the comparison
    * accuracy : angle acceptance (angle - accuracy < angle < angle + accuracy)
    **/
    public boolean printAndCompare(Hand currentHand, HandModel model, double accuracy)
    {
        // Create a model for the current hand
        HandModel currentModel = modelGenerator.generate(currentHand);

        // Print the hand with color validation via the both models
        drawer.draw(currentHand, model, currentModel, accuracy);

        // Return the comparaison result

        return true;
    }

}
/**
 * Utils for drawing hands.
 */
public class HandDrawer
{
    private PApplet parent;

    public HandDrawer(PApplet parent)
    {
        this.parent = parent;
    }

    public void draw(Hand hand)
    {
        draw(hand, false); 
    }

    public void draw(Hand hand, HandModel comparisonModel, HandModel currentModel, double accuracy)
    {
        // TODO
        draw(hand, false); 
    }

    public void draw(Hand hand, boolean hightlightIndex)
    {
        for(Finger finger : hand.getFingers())
        {
            drawJoints(finger, 5, hightlightIndex);

            int nbBones = finger.getType() == 0 ? 2 : 3;

            for(int i = 0; i < nbBones; i++)
            {
                drawBone(finger.getBone(i));
            }      
        }
        drawPalm(hand);
    }

    public void drawPalm(Hand hand)
    {
        parent.fill(150,150,150, 150);
        this.parent.strokeWeight(10);

        PVector lastJointOfThumb = hand.getThumb().getProximalBone().getPrevJoint();
        PVector lastJointOfIndex = hand.getIndexFinger().getMetacarpalBone().getPrevJoint();
        PVector lastJointOfMiddle = hand.getMiddleFinger().getMetacarpalBone().getPrevJoint();
        PVector lastJointOfRing = hand.getRingFinger().getMetacarpalBone().getPrevJoint();
        PVector lastJointOfPinky = hand.getPinkyFinger().getMetacarpalBone().getPrevJoint();

        PVector lastJointOfThumb2 = hand.getThumb().getProximalBone().getNextJoint();
        PVector lastJointOfIndex2 = hand.getIndexFinger().getMetacarpalBone().getNextJoint();
        PVector lastJointOfMiddle2 = hand.getMiddleFinger().getMetacarpalBone().getNextJoint();
        PVector lastJointOfRing2 = hand.getRingFinger().getMetacarpalBone().getNextJoint();
        PVector lastJointOfPinky2 = hand.getPinkyFinger().getMetacarpalBone().getNextJoint  ();

        this.parent.beginShape();

        this.parent.vertex(lastJointOfThumb.x, lastJointOfThumb.y);
        this.parent.vertex(lastJointOfIndex.x, lastJointOfIndex.y);
        this.parent.vertex(lastJointOfMiddle.x, lastJointOfMiddle.y);
        this.parent.vertex(lastJointOfRing.x, lastJointOfRing.y);
        this.parent.vertex(lastJointOfPinky.x, lastJointOfPinky.y);

        this.parent.vertex(lastJointOfPinky2.x, lastJointOfPinky2.y);
        this.parent.vertex(lastJointOfRing2.x, lastJointOfRing2.y);
        this.parent.vertex(lastJointOfMiddle2.x, lastJointOfMiddle2.y);
        this.parent.vertex(lastJointOfIndex2.x, lastJointOfIndex2.y);
        this.parent.vertex(lastJointOfThumb2.x, lastJointOfThumb2.y);

        this.parent.endShape(PConstants.CLOSE);
    }

    public void drawJoints(Finger finger, float radius, boolean hightlightIndex)
    {
        this.parent.strokeWeight(10);
        this.parent.noStroke();
        this.parent.fill(0);

        PVector tip = finger.getPositionOfJointTip();
        PVector mcp = finger.getPositionOfJointMcp();
        PVector pip = finger.getPositionOfJointPip();
        PVector dip = finger.getPositionOfJointDip();

        this.parent.ellipseMode(PConstants.CENTER);
        this.parent.ellipse(pip.x, pip.y, radius, radius);
        this.parent.ellipse(dip.x, dip.y, radius, radius);
        this.parent.ellipse(tip.x, tip.y, radius, radius);

        if (finger.getType() != 0) 
        { // \u2260thumb
            this.parent.ellipse(mcp.x, mcp.y, radius, radius);
        }

        if(hightlightIndex && finger.getType() == EFingerReferences.INDEX.getIndex())
        {
            this.parent.stroke(255, 0, 0);
            this.parent.strokeWeight(3);
            this.parent.noFill();
            this.parent.ellipse(tip.x, tip.y, 25, 25);
            this.parent.noStroke();
        }
    }

    public void drawBone(Bone bone)
    {
        this.parent.noFill();

        PVector next = bone.getNextJoint();
        PVector prev = bone.getPrevJoint();

        this.parent.noFill();
        this.parent.stroke(150,150,150,150);
        this.parent.strokeWeight(20);
        this.parent.strokeJoin(ROUND);

        this.parent.beginShape(PConstants.LINES);
        this.parent.vertex(next.x, next.y);
        this.parent.vertex(prev.x, prev.y);
        this.parent.endShape(PConstants.OPEN);
    }
}
/**
  * Utils for hand model generation.
 **/
public class HandModelGenerator
{
  public HandModel generate(Hand hand)
  {
    HandModel handModel = new HandModel();
    
    for(Finger finger : hand.getFingers())
    {
      BoneDetectionModel boneDetection = null;
      Bone previousBone = null;
      Bone currentBone = null;
      
      int nbBones = finger.getType() == 0 ? 3 : 4;
      
      for(int i = 0; i < nbBones; i++)
      {
        currentBone = finger.getBone(i);
        
        if(previousBone != null)
        {
          boneDetection = detect(finger, previousBone, currentBone);
          handModel.addBoneDetection(boneDetection);
        }
        
        previousBone = currentBone;
      }
      
      // Create a dependency model for last bone
      handModel.addBoneDetection(new BoneDetectionModel(currentBone.getType(), boneDetection)); 
    }
    
    return handModel;
  }
  
  private BoneDetectionModel detect(Finger finger, Bone previousBone, Bone currentBone)
  {
    if(previousBone == null)
    {
      return null;
    }
    
    double angle = getAngle(previousBone.getPrevJoint(), previousBone.getNextJoint(), currentBone.getNextJoint());
    BoneDetectionModel detection = new BoneDetectionModel(angle, finger.getType(), previousBone.getType());
    
    return detection;
  }
   
  private Double getAngle(PVector p1, PVector p2, PVector p3)
  {
    Double alpha = Math.atan2(p1.y - p2.y, p1.x - p2.x);
    Double beta = Math.atan2(p3.y - p2.y, p3.x - p2.x);
    Double angle = (beta - alpha);
  
    //Correction de l'angle pour le resituer entre 0 et 2PI
    while (angle < 0.0f || angle > 2 * Math.PI)
    {
      if (angle < 0.0f)
      {
          angle += 2 * Math.PI;
      }
      else if (angle > 2 * Math.PI) 
      {
        angle -= 2 * Math.PI;
      }
    }
    
    return angle;
  }
}


/**
 * Not used anymore
 * Interface for button listeners
 */
@Deprecated
interface IButtonListener
{
    public void onButtonReleased();
    public void onButtonPressed();
}
/**
 * Interface defining all the interfaces used 
 * 
 */
/**
 * Describes a scene 
 */
interface IScene
{
    public void draw();

    //void mousePressed();

    //void mouseReleased();

}
/**
 * Represents the main title (aka menu) alowing user to play or exit
 * - many things commented for code history
 */
class MainTitle implements IScene
{
    private String title;
    //private Button enterButton, exitButton;
    private TimeoutButton enterButton, exitButton;
    ArrayList<PVector> cursors = new ArrayList<PVector>();
    PImage logo;

    public MainTitle()
    {
        logo = loadImage("logo.png");
        //enterButton = new Button(width/2 - width/16, height/2 - height/22.5, width/8, height/15, color(12,12,12), color(176,176,176));
        //exitButton = new Button(width/2 - width/16, height/2 + height/22.5, width/8, height/15, color(255,0,0), color(0,255,0));

        int buttonsWidth = width/8;
        int buttonsHeight = height/15;

        int firstButtonX = width/2 - buttonsWidth/2;
        int firstButtonY = height/2 - buttonsHeight/2;

        enterButton = new TimeoutButton(firstButtonX, firstButtonY, buttonsWidth, buttonsHeight, 1, "Play", color(0,255,0));
        exitButton = new TimeoutButton(firstButtonX, firstButtonY + buttonsHeight + buttonsHeight/2, buttonsWidth, buttonsHeight, 1, "Exit");

        /*
        IButtonListener enterButtonEventsListener = new EnterButtonEventsListener(enterButton);
        enterButton.setButtonEventsListener(enterButtonEventsListener);
        IButtonListener exitButtonEventsListener = new ExitButtonEventsListener(exitButton);
        exitButton.setButtonEventsListener(exitButtonEventsListener);
        */
    }

    public MainTitle(String title)
    {
        this();
        this.title = title;
    }

    private void drawText()
    {
        textSize(64);
        fill(0);
        textAlign(CENTER, CENTER);
        image(logo, width/2 - logo.width/2, height/4 - logo.height/2);
        //text("EXIT", exitButton.x + exitButton.w/2, exitButton.y + exitButton.h/2);
        //fill(255);
        //text("PLAY", enterButton.x + enterButton.w/2, enterButton.y + enterButton.h);
    }

    private void drawButton()
    {
        //enterButton.draw();
        //exitButton.draw();
        enterButton.render();
        exitButton.render();
    }

    public void draw()
    {
        background(0);
        cursors.clear();
        drawButton();
        drawText();

        strokeWeight(3);
        noFill();
        for (Hand hand : leap.getHands())
        {
            Finger index = hand.getIndexFinger();
            PVector pos = index.getPosition();
            if (hand.isRight())
            {
                stroke(255, 0, 0);
            } 
            else
            {
                stroke(0, 255, 0);
            }
            ellipse(pos.x, pos.y, 30, 30);
            cursors.add(pos);
        }

        enterButton.update(cursors);
        exitButton.update(cursors);

        if(enterButton.showPress == 10)
        {
            currentDisplay = new SecondaryScene("Toto");
        }
        if(exitButton.showPress == 10)
        {
            System.exit(0);
        }
    }
    /*
    public void mousePressed()
    {
        enterButton.buttonPressed();
        exitButton.buttonPressed();
    }
    */
    /*
    public void mouseReleased()
    {
        enterButton.buttonReleased();
        exitButton.buttonReleased();
    }
    */
}



class RecordScreen implements IScene
{ 
  private PApplet env;
  private boolean eventLocked;
  
  float rect_x = width/4;
  float rect_y = height/4;
  float rect_w = width/2;
  float rect_h = width/4;
  
  float btn_w = width/8;
  float btn_h = width/16;
  float btn_x = width/2 - btn_w / 2;
  float btn_y = 9*height/10 - btn_h / 2;
  
  public RecordScreen(PApplet environment)
  {
    env = environment;
    eventLocked = false;
  }
  
  public void saveModel(Hand hand)
  {
    HandModelGenerator generator = new HandModelGenerator();
    HandModel handModel = generator.generate(hand);
    
    Scanner scanner = new Scanner(System.in);
    String letter = scanner.nextLine();
    
    handModel.setLetter(letter);
    
    ObjectOutputStream oos = null;

    try {
      final FileOutputStream fichier = new FileOutputStream(sketchPath("mon_objet.ser"));
      oos = new ObjectOutputStream(fichier);
      oos.writeObject(handModel);
    } catch (final java.io.IOException e) {
      e.printStackTrace();
    } finally {
      try {
        if (oos != null) {
          oos.flush();
          oos.close();
          
          print("Hand saved");
        }
      } catch (final IOException ex) {
        ex.printStackTrace();
      }
    }
  }
  
  public void manageBtnClick(Hand hand)
  {
    if(mousePressed)
    {
      if(!eventLocked && mouseX > btn_x && mouseX < btn_x + btn_w && mouseY > btn_y && mouseY < btn_y + btn_h)
      {
        eventLocked = true;
        saveModel(hand);
        eventLocked = false;
      }
    }
  }
  
  public void draw(){
    background(0);
    
    fill(255);
    rect(rect_x, rect_y, rect_w, rect_h);
    
    for(Hand hand : leap.getHands())
    {
      HandDrawer drawer = new HandDrawer(env);
      drawer.draw(hand);
      manageBtnClick(hand);
    }
    
    fill(255);
    rect(btn_x, btn_y, btn_w, btn_h);
    fill(0);
    textAlign(CENTER, CENTER);
    text("Record", btn_x + btn_w/2, btn_y + btn_h/2);
    
    
    
  }
}
/**
 * Second scene: will represent the game
 */
class SecondaryScene implements IScene
{

    private TimeoutButton back;
    ArrayList<PVector> cursors = new ArrayList<PVector>();
    private String title;

    public SecondaryScene(String title)
    {
        this.title = title;
        int buttonsWidth = width/8;
        int buttonsHeight = height/15;

        int buttonX = buttonsWidth/2;
        int buttonY = buttonsHeight;
        back = new TimeoutButton(buttonX, buttonY, buttonsWidth, buttonsHeight, 1, "Back");
    }

    private void drawText()
    {
        textSize(64);
        fill(0);
        textAlign(CENTER);
        text(title, width/2, height/5);
    }

    public void draw()
    {
        background(0);
        cursors.clear();
        back.render();
        drawText();

        strokeWeight(3);
        noFill();
        for (Hand hand : leap.getHands())
        {
            Finger index = hand.getIndexFinger();
            PVector pos = index.getPosition();
            if (hand.isRight())
            {
                stroke(255, 0, 0);
            }
            else
            {
                stroke(0, 255, 0);
            }
            ellipse(pos.x, pos.y, 30, 30);
            cursors.add(pos);
        }

        back.update(cursors);

        if(back.showPress == 10)
        {
            currentDisplay = new MainTitle();
        }
    }

    /*
    public void mousePressed()
    {
    }
    */
    /*
    public void mouseReleased()
    {
    }
    */
}
/*
 * Got this code from http://michaelkipp.de/interaction/leap.html
 */ 
class TimeoutButton {

    int x;
    int y;
    int w;
    int h;
    float timeout;
    String text;
    int color_;

    boolean inside;
    int startFrame; 
    int showPress = 0;
    float secInside = -1;

    TimeoutButton(int x, int y, int w, int h, float timeout, String text) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.timeout = timeout;
        this.text = text;
        this.color_ = -1;
    }

    TimeoutButton(int x, int y, int w, int h, float timeout, String text, int color_) {
        this(x, y, w, h, timeout, text);
        this.color_ = color_;
    }

    public void render() {

        // white after pressed
        if (showPress > 0) {
            fill(255);
            showPress--;
        } else {
            fill(100);
        }
        noStroke();
        rect(x, y, w, h);

        // show progress in red
        if (secInside > 0 && showPress <= 0) {
            if(color_ != -1)
                fill(color_);
            else
                fill(255, 0, 0);
            rect(x, y, secInside/timeout * w, h);
        }

        fill(255);
        textAlign(CENTER);
        textSize(30);
        text(text, x + w/2, y + h/2 + 15);
    }

    public boolean oneIsIn(ArrayList<PVector> points) {
        for (PVector v : points) {
            if (v.x >= x && v.x <= x + w && v.y >= y && v.y <= y + h) {
                return true;
            }
        }
        return false;
    }

    public void update(ArrayList<PVector> cursors) {
        if (oneIsIn(cursors)) {
            if (inside) {
                secInside = (PApplet.parseFloat(frameCount - startFrame)) / frameRate;
                if (secInside > timeout) {
                    startFrame = frameCount;
                    pressed();
                }
            } else {
                inside = true;
                startFrame = frameCount;
            }
        } else {
            inside = false;
            secInside = -1;
        }
    }

    public void pressed() {
        showPress = 10;
        secInside = -1;
    }
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Sign" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
