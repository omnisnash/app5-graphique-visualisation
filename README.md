# Graphisme et visualisation - Projet

*Polytech' Paris-Sud - APP5 INFO*

Membres :

* Thomas Cottin
* Clément Garin
* Léo Donny

## Contexte

Le projet consite à créer une application contenant une scène 3D, avec
pour objectif d'interagir avec l'utilisateur par un autre biais que le duo
clavier/souris.

## Présentation

Nous avons choisis d'utiliser la technologie du Leapmotion. L'idée est
d'apprendre la langue des signes à l'utilisateur.

L'apprentissage se limite à certaines lettres de l'alphabet. En effet,
la capacité de detection du Leapmotion se limite aux mains. On ne peut donc
pas détecter des mots, car cela demande de capter l'ensemble du corps.
