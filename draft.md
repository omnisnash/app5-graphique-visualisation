Draft pour le rapport

# Menu de l'application

**Première phase :**

* Construction du pseudo "scenegraph"
* Deux boutons :
    * Lancer le mode entrainement
    * Quitter l'application
* Interaction via clavier/souris

**Seconde phase :**

* Interaction avec les boutons via le Leapmotion

# Validation du modèle

*Modèle = position à reproduire avec la main, représentant une lettre du language des signes*

Pour valider le modèle effectué par l'utilisateur deux solution :

**Comparaison à un pseudo-modèle** : 

on défini le modèle à reproduire et on le considère comme étant à l'echelle 1:1. Lorsqu'une personne veut essayer l'application, elle doit d'abord passer une phase de calibration en présentant sa main face au Leapmotion. Ainsi, on peut  calculer les différences avec le modèle 1:1 en comparant les extrémités des os de la main.

Il faut donc définir un modèle relativement précis, en prenant en compte les différentes positions des doigts dans l'espace.

**Comparaison via les angles** :

Plus simple, le modèle est représenté par des angles entre les différentes phalanges.

[Image]

